import redis, json

class PyRedis:
    def __init__(self,):
        self.redis = redis.Redis()
    
    def set_key(self, key, value, expiry):
        self.redis.setex(key, expiry, json.dumps(value))
    
    def get_key(self, key): 
        value = self.redis.get(key)
        return json.loads(value) if value != None else None
        
    def delete_key(self, key):
        self.redis.delete(key)
    
    def increament(self, key, value = 1):
        self.redis.incrby(key, value)
    
    def exists(self, key):
        return True if self.redis.exists(key) == 1 else False

    def close(self):
        self.redis.close()