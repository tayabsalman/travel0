SECONDS = {
    "1_MINUTE": 60,
    "5_MINUTES": 5 * 60,
    "10_MINUTES": 10 *60,
    "1_MONTH": 1*31*24*60*60
}