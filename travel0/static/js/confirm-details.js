$(document).ready(function(){
    $(".js-edit-locations").unbind().bind("click", function(){
        window.location.href = "/";
    });

    $(".js-edit-selections").unbind().bind("click", function(){
        window.location.href = "/car-driver";
    });

    $(".js-proceed").unbind().bind("click", function(){
        window.location.href = "/proceed-to-pay";
    });
});