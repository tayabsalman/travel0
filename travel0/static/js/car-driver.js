$(document).ready(function(){
    $(".js-driver-filter").unbind().bind("click", function(e){
        $('#driver-filter-modal').modal("show");
    });

    $(".btn-driver-filter-apply").unbind().bind("click", function(){
        var selectedLanguages = [];
        $.each($("#driver-filter-modal").find("input[type='checkbox']:checked"),function(){
            selectedLanguages.push($(this).val());
        });
        ApplyFilters(selectedLanguages);
    });

    $(".btn-driver-filter-reset").unbind().bind("click", function(){
        $("#driver-filter-modal").find("input[type='checkbox']:checked").prop("checked", false);
        ApplyFilters();
    });

    $(".js-proceed").unbind().bind("click", function(){
        var selectedCarId = $('#car-selection').val();
        var selectedDriverId = $(".driver-card[driver-selected='1']").attr("driver-id");

        if(selectedCarId == undefined)
        {
            alert("Please select a car of your choice");
            return ;
        }
        if(selectedDriverId == undefined)
        {
            alert("Please select a Driver of your choice");
            return ;
        }

        var selectedFilters = [];
        $.each($("#driver-filter-modal").find("input[type='checkbox']:checked"),function(){
            selectedFilters.push($(this).val());
        });
        var data = {};
        data["selected_car"] = selectedCarId;
        data["selected_driver"] = selectedDriverId;
        data["selected_filters"] = JSON.stringify(selectedFilters);
        console.log(data);
        Framework.Ajax("save_selections", data, function(response){
            if(response.result.state == "ok")
                window.location.href = "confirm-details";
            else
                alert(response.result.meta)
        });
    });
    BindEventsOnNewData();
});
function BindEventsOnNewData()
{
    $(".js-select-driver").unbind().bind("click", this, function(e){
        e.preventDefault();

        var wasSelected= parseInt($(this).closest(".driver-card").attr("driver-selected"));

        if(wasSelected)
        {
            $(this).closest(".driver-card").removeClass("bg-info").attr("driver-selected", 0);
            $(this).closest(".driver-card").find(".js-selected-label").hide();
        }
        else
        {
            $(".js-select-driver").closest(".driver-card[driver-selected='1']").find(".js-selected-label").hide();
            $(".js-select-driver").closest(".driver-card[driver-selected='1']").attr("driver-selected", 0).removeClass("bg-info");
            $(this).closest(".driver-card").find(".js-selected-label").show();
            $(this).closest(".driver-card").addClass("bg-info").attr("driver-selected", 1);
        }
        selectedDriverId = $(".driver-card[driver-selected='1']").attr("driver-id");
    });
}
function ApplyFilters(language_ids = [])
{
    if(language_ids.length > 0)
    {
        $(".js-filter-count").html("(" + language_ids.length + ")");
        $(".js-filter-count").show();
    }
    else
        $(".js-filter-count").hide();
    
    var data = {};
    data["language_ids"] = JSON.stringify(language_ids);
    Framework.Ajax("filter_drivers", data, function(response){
        if(response.result.state == "ok")
        {
            $(".js-drivers-container").empty();
            $(".js-drivers-container").html(response.data);
            $("#driver-filter-modal").modal("hide");
            BindEventsOnNewData();
            if(selectedDriverId != undefined)
            {
                $(".driver-card[driver-id='" + selectedDriverId + "']").addClass("bg-info");
                $(".driver-card[driver-id='" + selectedDriverId + "']").attr("driver-selected", 1);
                $(".driver-card[driver-id='" + selectedDriverId + "']").find(".js-selected-label").show();
            }
        }
        else
            alert(response.result.meta);
    });
}