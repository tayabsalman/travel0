$(document).ready(function(){
    $(".js-proceed").unbind().bind("click", function(e){
        e.preventDefault();
        var fromAddress = $('#from-address').val().trim();
        if(fromAddress.length < 1)
            return alert("Please provide a valid from address");
        
            var toAddress = $('#to-address').val().trim();
        if(toAddress.length < 1)
            return alert("Please provide a valid T0 address");
        
        var pickupDate = $("#datepicker1").val();
        var pickupTime = $("#timepicker1").val();
        var correctDateFormat = /^\d{4}[\-]\d{2}[\-]\d{2}$/;
        var correctTimeFormat = /^\d{2}:\d{2} ..$/;
        if(!pickupDate.match(correctDateFormat))
            return alert("Please select a valid pickup date");
        if(!pickupTime.match(correctTimeFormat))
            return alert("Please select a valid pickup time");
        
        var scheduledPickup = pickupDate + " " + pickupTime;
        var twoHoursFromNow = new Date();
        twoHoursFromNow.setHours(twoHoursFromNow.getHours() + 2);
        if(twoHoursFromNow > new Date(pickupDate + " " + pickupTime))
            return alert("Pickup time should be atleast 2 hours from now");

        data = {}
        data["from_address"] = fromAddress;
        data["to_address"] = toAddress;
        data["pickup_date"] = pickupDate;
        data["pickup_time"] = pickupTime;
        Framework.Ajax("save_locations", data, function(response){
            if(response.result.state == "ok")
                window.location.href = "/car-driver"
            else
                alert(response.result.meta)
        });
    });

    var currentDate = new Date();
    $('#datepicker1').datepicker({
        dateFormat: "yy-mm-dd",
        minDate : currentDate
    });
    $('#timepicker1').timepicker();
});