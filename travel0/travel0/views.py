from django.shortcuts import render, redirect
from .controller import Controller
from django.http import JsonResponse, HttpResponse, HttpResponseNotFound

from django.contrib.auth.decorators import login_required
import json
from django.contrib.auth import logout as log_out
from django.conf import settings
from django.http import HttpResponseRedirect
from urllib.parse import urlencode

# @login_required
# def dashboard(request):
#     user = request.user
#     auth0user = user.social_auth.get(provider='auth0')
#     userdata = {
#         'user_id': auth0user.uid,
#         'name': user.first_name,
#         'picture': auth0user.extra_data['picture'],
#         'email': auth0user.extra_data['email'],
#     }

#     return render(request, 'dashboard.html', {
#         'auth0User': auth0user,
#         'userdata': json.dumps(auth0user.extra_data, indent=4)
#     })

def logout(request):
    log_out(request)
    return_to = urlencode({'returnTo': request.build_absolute_uri('/')})
    logout_url = 'https://%s/v2/logout?client_id=%s&%s' % \
                 (settings.SOCIAL_AUTH_AUTH0_DOMAIN, settings.SOCIAL_AUTH_AUTH0_KEY, return_to)
    return HttpResponseRedirect(logout_url)

def view_homepage(request):
    if security_checkup(request) != True:
        return HttpResponseNotFound("<h1>You have been blocked for some security reason</h1>")
    
    controller_object = Controller()
    controller_object.ip_address = get_client_ip(request)
    context = controller_object.home_page()
    context["tab_title"] = "Travel0 - Easy Trip Planner"
    return render(request, "home.html", context)

def view_car_driver(request):
    if security_checkup(request) != True:
        return HttpResponseNotFound("<h1>You have been blocked for some security reason</h1>")
    
    controller_object = Controller()
    controller_object.ip_address = get_client_ip(request)
    context = controller_object.car_driver_page()
    if "redirect" in context.keys() and context["redirect"] is not None:
        return redirect(context["redirect"])
    context["tab_title"] = "Choose your car and driver"
    return render(request, "car-driver.html", context)
    
def view_confirm_details(request):
    if security_checkup(request) != True:
        return HttpResponseNotFound("<h1>You have been blocked for some security reason</h1>")
    
    if not request.user.is_authenticated:
        return redirect("/login/auth0")
        
    controller_object = Controller()
    controller_object.ip_address = get_client_ip(request)
    controller_object.request = request
    context = controller_object.confirm_details_page()
    if "redirect" in context.keys() and context["redirect"] is not None:
        return redirect(context["redirect"])
    context["tab_title"] = "Confirm your trip details"
    return render(request, "confirm-details.html", context)

def view_proceed_to_pay(request):
    if security_checkup(request) != True:
        return HttpResponseNotFound("<h1>You have been blocked for some security reason</h1>")
    
    return HttpResponse("<h1>Payment Gateway Page</h1>")

def view_ajax(request):
    response = {}
    response["data"] = ""
    response["result"] = {
        "state" : "ok",
        "meta" : ""
    }
    if security_checkup(request) != True:
        response["result"]["state"] = "invalid_request"
        response["result"]["meta"] = "You have been locked out of system for security reasons"
        return JsonResponse(response)

    controller_object = Controller()
    controller_object.ip_address = get_client_ip(request)
    controller_object.request = request
    try:
        response["data"] = getattr(controller_object, request.POST["activity"])(request.POST)
    except Exception as ex:
        print(ex)
        response["result"] = {
            "state" : "invalid_request",
            "meta" : str(ex)
        }

    return JsonResponse(response)

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def security_checkup(request):
    controller_object = Controller()
    controller_object.ip_address = get_client_ip(request)
    attacked = controller_object.check_for_attacks()

    if attacked:
        return False
    
    controller_object.update_request_count()
    return True