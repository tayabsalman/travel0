from .entity import Entity
from library.pyredis import PyRedis
from library.const import *

from django.shortcuts import render
import json

class Controller:
    ip_address = "0.0.0.0"
    request = None

    def home_page(self):
        entity_object = Entity()
        entity_object.ip = self.ip_address
        return entity_object.get_saved_locations()
    
    def car_driver_page(self):
        entity_object = Entity()
        entity_object.ip = self.ip_address

        if len(entity_object.get_saved_locations()) == 0:
            return {"redirect": "/"}
        
        context = entity_object.get_saved_selections()
        if len(context) > 0:
            context["selected_car"] = int(context["selected_car"])
            context["selected_driver"] = int(context["selected_driver"])
            context["selected_filters"] = [int(x) for x in context["selected_filters"]]
        else:
            context["selected_car"] = -1
            context["selected_driver"] = -1
            context["selected_filters"] = []
        context["drivers"] = entity_object.filter_drivers(context["selected_filters"])
        context["languages"] = entity_object.get_all_languages()
        return context

    def confirm_details_page(self,):
        entity_object = Entity()
        entity_object.ip = self.ip_address
        context = {}
        saved_locations = entity_object.get_saved_locations()
        if len(saved_locations) == 0:
            return {"redirect" : "/"}
        context["from_address"] = saved_locations["from_address"]
        context["to_address"] = saved_locations["to_address"]
        context["pickup_data"] = "10-10-2020" #saved_locations["pickup_data"]
        context["pickup_time"] = "10:00:00" #saved_locations["pickup_time"]

        saved_selections = entity_object.get_saved_selections()
        if len(saved_selections) == 0:
            return {"redirect" : "/car-driver"}
        context["selected_car"] = saved_selections["selected_car"]
        context["selected_driver"] = saved_selections["selected_driver"]
        context["selected_filters"] = saved_selections["selected_filters"]

        user_data = self.get_user_data()
        context["user_name"] = user_data["name"]
        context["user_email"] = user_data["email"]
        return context

    def get_user_data(self, ):
        user = self.request.user
        auth0user = user.social_auth.get(provider='auth0')
        userdata = {
            'name': user.first_name,
            'email': auth0user.extra_data['email'],
        }
        return userdata
    
    def check_for_attacks(self):
        redis_key = f"{self.ip_address}-count"
        redis_obj = PyRedis()
        req_count_per_min = redis_obj.get_key(redis_key)
        redis_obj.close()
        if req_count_per_min != None and req_count_per_min >= 100:
            return True
        return False
    
    def update_request_count(self):
        redis_key = f"{self.ip_address}-count"
        redis_obj = PyRedis()
        if redis_obj.exists(redis_key):
            redis_obj.increament(redis_key)
        else:
            redis_obj.set_key(redis_key, 1, SECONDS["1_MINUTE"])
        redis_obj.close()

    def save_locations(self, data):
        entity_object = Entity()
        entity_object.ip = self.ip_address
        save_data = {
            "from_address" : data["from_address"],
            "to_address" : data["to_address"],
            "pickup_date" : data["pickup_date"],
            "pickup_time" : data["pickup_time"]
        }
        return entity_object.save_locations(save_data)

    def save_selections(self,data):
        if data["selected_driver"] is None:
            raise Exception("Missing param selected_driver")
        if data["selected_car"] is None:
            raise Exception("Missing param selected_car")
        if data["selected_filters"] is None:
            raise Exception("Missing param OPTIONAL")

        data = {
            "selected_car" : data["selected_car"],
            "selected_driver" : data["selected_driver"],
            "selected_filters" : json.loads(data["selected_filters"])
        }
        entity_object = Entity()
        entity_object.ip = self.ip_address
        return entity_object.save_selections(data)

    def filter_drivers(self, data):
        if data["language_ids"] is None:
            raise Exception("Param 'language_ids' is missing")

        entity_object = Entity()
        drivers = entity_object.filter_drivers(json.loads(data["language_ids"]))
        context = {
            'drivers': drivers
        }
        return self.get_rendered_html("ajax/driver-filter.html", context)
    
    def get_rendered_html(self, template_name, context = None):
        return render(self.request, template_name, context, content_type = "text/html").content.decode("utf-8")
    
    def save_detailes(self):
        return Trues