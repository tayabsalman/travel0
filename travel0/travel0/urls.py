"""travel0 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from .views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('django.contrib.auth.urls')),
    path('', include('social_django.urls')),
    path('logout', logout),
    path('', view_homepage),
    path('car-driver', view_car_driver),
    path('confirm-details', view_confirm_details),
    path('proceed-to-pay', view_proceed_to_pay),
    path('ajax', view_ajax),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urlpatterns = [
#     path('', index),
#     path('dashboard', dashboard),
#     path('logout', logout),
#     path('', include('django.contrib.auth.urls')),
#     path('', include('social_django.urls')),
# ]