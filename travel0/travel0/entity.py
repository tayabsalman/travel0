from library.const import *
from library.pyredis import PyRedis
from travel1.models import *
import json
class Entity:
    def get_saved_locations(self):
        #connect to redis and get data
        redis_key = f"{self.ip}-locations"
        redis_obj = PyRedis()
        saved_locations = redis_obj.get_key(redis_key)
        print(self.ip)
        print(saved_locations)
        redis_obj.close()
        result = {}
        if saved_locations != None:
            result["from_address"] = saved_locations["from_address"]
            result["to_address"] = saved_locations["to_address"]
            result["pickup_date"] = saved_locations["pickup_date"]
            result["pickup_time"] = saved_locations["pickup_time"]

        return result
    
    def save_locations(self, data):
        redis_key = f"{self.ip}-locations"
        redis_obj = PyRedis()
        redis_obj.set_key(redis_key, data, SECONDS["1_MONTH"])
        redis_obj.close()
        return True
    
    def get_saved_selections(self):
        #connect to redis and get data
        redis_key = f"{self.ip}-selections"
        redis_obj = PyRedis()
        saved_selections = redis_obj.get_key(redis_key)
        redis_obj.close()
        result = {}
        if saved_selections != None:
            return saved_selections
        return result
    
    def get_all_drivers(self, driver_ids = []):
        if len(driver_ids) > 0:
            drivers = Driver.objects.filter(id__in = driver_ids)
        else:
            drivers = Driver.objects.filter()

        processed_data = []
        for driver in drivers:
            driver = driver.__dict__
            del(driver["_state"])
            lang_ids = list(map(lambda x: x['language'], list(DriverLanguage.objects.filter(driver= driver['id']).values("language"))))
            driver["languages"] = list(map(lambda x: x['name'], list(Language.objects.filter(id__in=lang_ids).values("name"))))
            processed_data.append(driver)
        return processed_data
    
    def get_all_languages(self):
        languages = list(map(lambda x: x.__dict__, list(Language.objects.filter())))
        return languages

    def filter_drivers(self, languages = []):
        driver_ids = list(map(lambda x: x["driver"], list(DriverLanguage.objects.filter(language__in = languages).values("driver"))))
        drivers = []
        if len(driver_ids) > 0 or len(languages) == 0 :
            drivers = self.get_all_drivers(driver_ids)
        return drivers
    
    def save_selections(self, data):
        redis_key = f"{self.ip}-selections"
        redis_obj = PyRedis()
        redis_obj.set_key(redis_key, data, SECONDS["1_MONTH"])
        redis_obj.close()
        return True
    
    def close(self):
        print("Close any connections")
