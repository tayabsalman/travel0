from django.db import models

STATUS = (
    (0,"Draft"),
    (1,"Publish")
)

class Language(models.Model):
    name = models.CharField(max_length = 45)

    class Meta:
        ordering = ['id']
        verbose_name_plural = "Languages"
    
    def __str__(self):
        return self.name

class Driver(models.Model):
    name = models.CharField(max_length = 45)
    image = models.ImageField(upload_to = "Drivers/images")
    index = models.IntegerField(default=0)
    status = models.IntegerField(choices = STATUS)
    created = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)

    class Meta:
        ordering = ['-index']
        verbose_name_plural = "Drivers"
    
    def __str__(self):
        return self.name

class DriverLanguage(models.Model):
    driver = models.ForeignKey(Driver, on_delete = models.SET_NULL, null = True)
    language = models.ForeignKey(Language, on_delete = models.CASCADE, null =True)

    def __str__(self):
        return f"#{self.driver.id}({self.driver.name})-({self.language.name})"