from django.contrib import admin
from .models import *

class DriverAadmin(admin.ModelAdmin):
    list_display = ['id', 'index', 'name', 'status']
    list_filter = ['status']
    search_fields = ['name']
    list_editable = ['index', 'status']
admin.site.register(Driver, DriverAadmin)

class LanguageAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
admin.site.register(Language, LanguageAdmin)

class DriverLanguageAdmin(admin.ModelAdmin):
    list_display = ['driver', 'language']
admin.site.register(DriverLanguage, DriverLanguageAdmin)
# Register your models here.
